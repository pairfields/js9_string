"use strict";
// 1 Первую часть задания повторить по уроку
// 2 Создать функцию showMyDB, которая будет проверять свойство privat. Если стоит в позиции false - выводит в консоль главный объект программы
// 3 Создать функцию writeYourGenres в которой пользователь будет 3 раза отвечать на вопрос 'Ваш любимый жанр под номером ${номер по порядку}'. Каждый ответ записывается в массив данных genres


//1

// функции
let num = 20;

function showFirstMessage(text){
	console.log(text);
	console.log(num);
}

showFirstMessage("Hello World!!!");
console.log(num);

function calc(a,b){
	return(a+b);
}

console.log(calc(4,3));
console.log(calc(5,6));
console.log(calc(10,6));

function ret(){
	let num = 50;


	return num;
}

const anotherNum = ret();
console.log(anotherNum);

const calc2 = (a,b) => a+b;

//строки

const str ="test"

console.log(str.length);
console.log(str.toUpperCase());
console.log(str.toLowerCase());

const fruit = "Some fruit";
console.log(fruit.indexOf("fruit"));

const logg = "Hello world";
console.log(logg.slice(6, 11));
console.log(logg.substring(6, 11));

const num2 = 12.2;
console.log(Math.round(num));

const test = "12.2px";
console.log(parseFloat(test))

//2
// я не совсем понял что значит главный объект программы,
// но учитывая название функции вероятнее всего речь идет об объекте, который мы создавали в предыдущем задании

const personalMovieDB = {
	count: 5,
	movies: {
			logan:8.2,
			wall_e: 10},
	actors: {},
	genres: [],
	private: false
};

function showMyDB(){
	if(personalMovieDB.private == false){
			console.log(personalMovieDB);
	}
}

// 3

function writeYourGenres(){
	for(let i = 1; i <= 3; i++){
			const genres = prompt(`Ваш любимый жанр под номером ${i}`);
			personalMovieDB.genres[i - 1] = genres;
	}
}

writeYourGenres()
